# 3DE4.script.name:    Set Focal Length to 'Adjust'
# 3DE4.script.version: v1.5
# 3DE4.script.gui:    Main Window:: James::Set
# 3DE4.script.comment: Adjusts the lens linked to the selected camera.
#
#James Porter

from tde4 import *
tde4.clearConsole()
tde4.updateGUI()


cam 		= tde4.getCurrentCamera()
lens		= tde4.getCameraLens(cam)

setParameterAdjustFlag(lens, "ADJUST_LENS_FOCAL_LENGTH","",1)