# -*- coding: utf-8 -*-
# 3DE4.script.name:    Toggle Sequence Cameras
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: Toggles sequences marked NOT stereo.
#
#James Porter
#
# v1
# July '14

from tde4 import *
tde4.clearConsole()

sc      = tde4.getCameraStereoMode(cam)
pg      = tde4.getCurrentPGroup()
c	= tde4.getCurrentCamera()
n	= tde4.getCameraNoFrames(c)
pl	= tde4.getPointList(pg,1)
camType = tde4.getCameraType(cam)

cam	= tde4.getCurrentCamera()
if cam!=None:
	type	= tde4.getCameraStereoMode(cam)
	if type=="STEREO_OFF" and camType=="SEQUENCE":
		while cam!=None:
			cam	= tde4.getNextCamera(cam)
			if cam==None: cam = tde4.getFirstCamera()
			type	= tde4.getCameraStereoMode(cam)
			camType = tde4.getCameraType(cam)
			if type=="STEREO_OFF" and camType=="SEQUENCE":
				tde4.setCurrentCamera(cam)
				tde4.updateGUI()
				break
