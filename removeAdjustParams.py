# 3DE4.script.name:    Set/Clear Parameter Adjustment Window
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Set
# 3DE4.script.comment: "Remove all parameters set to adjust"
#
#James Porter

from tde4 import *
tde4.clearConsole()
tde4.updateGUI()

removeAllAdjustParameters()
