# -*- coding: utf-8 -*-
# 3DE4.script.name:    Set Point Valid to "Always"
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Set
# 3DE4.script.comment: Turns selected points to active FOV
#
#James Porter

import tde4

pg    	= tde4.getCurrentPGroup()
points  = tde4.getPointList(pg, 1)

for p in points:
    tde4.setPointValidMode(pg,p, "POINT_VALID_ALWAYS")
