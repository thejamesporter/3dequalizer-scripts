# -*- coding: utf-8 -*-
# 3DE4.script.name:    Toggle Camera Enabled
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: toggles Turning the camera on/off.
#
#James Porter
#
# v1
# 19 October 2012

import tde4

cam_list = tde4.getCameraList(True)

if tde4.getCameraEnabledFlag(cam_list[0]) == True:
    for cam in cam_list:
        tde4.setCameraEnabledFlag(cam, False)
else:
    for cam in cam_list:
        tde4.setCameraEnabledFlag(cam, True)