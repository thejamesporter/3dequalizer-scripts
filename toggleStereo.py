# -*- coding: utf-8 -*-
# 3DE4.script.name:    Toggle Stereo Cameras
# 3DE4.script.version: v2.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: Toggles between primary and secondary eyes *not* sequences
#
#James Porter
#
# v2
# 19 October 2014

import tde4


c	= tde4.getCurrentCamera()
#pg      = tde4.getCurrentPGroup()
n	= tde4.getCameraNoFrames(c)
#pl	= tde4.getPointList(pg,1)
sc      = tde4.getCameraStereoMode(c)




cam	= tde4.getCurrentCamera()
if cam!=None:
	type	= tde4.getCameraStereoMode(cam)
	if type=="STEREO_PRIMARY":
		while cam!=None:
			cam	= tde4.getNextCamera(cam)
			if cam==None: cam = tde4.getFirstCamera()
			type	= tde4.getCameraStereoMode(cam)
			if type=="STEREO_SECONDARY":
				tde4.setCurrentCamera(cam)
				tde4.updateGUI()
				break
	elif type=="STEREO_SECONDARY":
		while cam!=None:
			cam	= tde4.getNextCamera(cam)
			if cam==None: cam = tde4.getFirstCamera()
			type	= tde4.getCameraStereoMode(cam)
			if type=="STEREO_PRIMARY":
				tde4.setCurrentCamera(cam)
				tde4.updateGUI()
				break