# 3DE4.script.name:    Set Distortion to 'Adjust' (Deg 2 & 4)
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Set
# 3DE4.script.comment: Adjusts the degree 2 and 4 of selected cameras lens
#
#James Porter

from tde4 import *
tde4.clearConsole()
tde4.updateGUI()

def getLDmodelParameterList(model):
    l = []
    for p in range(tde4.getLDModelNoParameters(model)):
        l.append(tde4.getLDModelParameterName(model, p))
    return l

cam 		= tde4.getCurrentCamera()
lens		= tde4.getCameraLens(cam)

model = tde4.getLensLDModel(lens)
for para in getLDmodelParameterList(model):
    value = tde4.getLensLDAdjustableParameter(lens, para, tde4.getLensFocalLength(lens), tde4.getLensFocus(lens))
    tde4.setLensDynamicDistortionFlag(lens, 0)
    value = tde4.setLensLDAdjustableParameter(lens, para, tde4.getLensFocalLength(lens), tde4.getLensFocus(lens), value)
    tde4.setLensDynamicDistortionFlag(lens, 1)
tde4.setLensDynamicDistortionFlag(lens, 0)
Dmodel1 = tde4.getLDModelParameterName(model,0)
Dmodel2 = tde4.getLDModelParameterName(model,3)
# typee	= tde4.getLDModelParameterType(model,Dmodel)
# adjust  = tde4.getLensLDAdjustableParameter(lens,Dmodel,1)

setLensDynamicDistortionMode(lens,"DISTORTION_STATIC")


setParameterAdjustFlag(lens, "ADJUST_LENS_DISTORTION_PARAMETER",Dmodel1,1)
setParameterAdjustFlag(lens, "ADJUST_LENS_DISTORTION_PARAMETER",Dmodel2,1)
