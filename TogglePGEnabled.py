# -*- coding: utf-8 -*-
# 3DE4.script.name:    Toggle PointGroup Enabled
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: toggles Turning the point group on/off.
#
#James Porter
#
#

import tde4

pg_list = tde4.getPGroupList(True)

try:

    if tde4.getPGroupEnabledFlag(pg_list[0]) == True:
        for pg in pg_list:
            tde4.setPGroupEnabledFlag(pg, False)
    else:
        for pg in pg_list:
            tde4.setPGroupEnabledFlag(pg, True)
except:
    print "No point group selected."
