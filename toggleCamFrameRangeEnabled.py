# -*- coding: utf-8 -*-
# 3DE4.script.name:    Toggle Cam Frame Range Enabled
# 3DE4.script.version: v2.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: Toggles the Frame range calculation to on/off\n and sets the frame range to the playback range
#
#James Porter
#
# v2
# 19 October 2014

import tde4

c	= tde4.getCurrentCamera()
f   = tde4.getCameraNoFrames(c)
p 	= tde4.getCameraPlaybackRange(c)
s	= tde4.getCameraNoFrames(c)

if tde4.getCameraFrameRangeCalculationFlag(c) ==1:
	tde4.setCameraFrameRangeCalculationFlag(c,0)
	tde4.setCameraCalculationRange(c,1,f)

else:
	tde4.setCameraFrameRangeCalculationFlag(c,1)
	tde4.setCameraCalculationRange(c,p[0],p[1])
