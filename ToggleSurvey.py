# 3DE4.script.name:    Toggle Survey Type
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: Toggles between survey modes.
# J Porter. 3rd July Toggles between survey modes. I couldn't find one already written, so I've
# modified the 'Toggle direction' script that Wolfgang Niedermeier wrote.

from tde4 import *

pg = getCurrentPGroup()
cam = getCurrentCamera()
nfr = getCameraNoFrames(cam)
points = getPointList(pg, 1)

if cam!=None and pg!=None:
    for point in points:
        if getPointSurveyMode(pg, point)=='SURVEY_FREE':
            setPointSurveyMode(pg, point, 'SURVEY_APPROX')
        elif getPointSurveyMode(pg, point)=='SURVEY_APPROX':
            setPointSurveyMode(pg, point, 'SURVEY_EXACT')
        elif getPointSurveyMode(pg, point)=='SURVEY_EXACT':
            setPointSurveyMode(pg, point, 'SURVEY_LINEUP')
        elif getPointSurveyMode(pg, point)=='SURVEY_LINEUP':
            setPointSurveyMode(pg, point, 'SURVEY_FREE')
