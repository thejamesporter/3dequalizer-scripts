# -*- coding: utf-8 -*-
# 3DE4.script.name:    Toggle Enhanced Tracking
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: toggles through Turning enhanced tracking for a point on or off.
#
#James Porter
#
# v1
# 19 October 2012

from tde4 import *

pg = getCurrentPGroup()
cam = getCurrentCamera()
nfr = getCameraNoFrames(cam)
points = getPointList(pg, 1)



for point in points:
  if getPointEnhancedTrackingFlag(pg, point)==1:
     setPointEnhancedTrackingFlag(pg, point, 0)
  elif getPointEnhancedTrackingFlag(pg, point)==0:
     setPointEnhancedTrackingFlag(pg, point, 1)