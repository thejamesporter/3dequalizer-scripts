# 3DE4.script.name:    Select Calculated Points
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Select
# 3DE4.script.comment: Selects points which are calculated in a point group
#
#James Porter

import tde4

pg     = tde4.getCurrentPGroup()
cam    = tde4.getCurrentCamera()
points = tde4.getPointList(pg, 0)
index  = tde4.getIndexPoint(pg,1)

#deselect every point
for point in points:
  tde4.setPointSelectionFlag(pg,point,0)

#find the type we want
for point in points:
  #if point is survey or approx, set to free. that way lineup stays as it is.
  if tde4.getPointCalculated3DStatus(pg, point) == 'CALCULATED':
    tde4.setPointSelectionFlag(pg,point,1)