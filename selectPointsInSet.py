# 3DE4.script.name:    Select all points in set
# 3DE4.script.version: v1
# 3DE4.script.gui:    Main Window:: James::Select
# 3DE4.script.comment: Short cut this. Select your set. Hit the shortcut and it will select all points in the set.
#
#James Porter
#
# v2
# 19 October 2014

import tde4

p 	= tde4.getCurrentPGroup()
s	= tde4.getSetList(p,1)

for sl in s:
	points = tde4.getSetPointList(p,sl)
	for po in points:
		tde4.setPointSelectionFlag(p,po,1)
