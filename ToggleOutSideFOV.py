# -*- coding: utf-8 -*-
# 3DE4.script.name:    Toggle Outside FOV
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: toggles Valid Outside FOV of tracking point for a point on or off.
#
#James Porter
#
# v1
# Feb 2013

from tde4 import *

pg = getCurrentPGroup()
cam = getCurrentCamera()
nfr = getCameraNoFrames(cam)
points = getPointList(pg, 1)



for point in points:
  if getPointValidOutsideFOVFlag(pg, point)==1:
     setPointValidOutsideFOVFlag(pg, point, 0)
  elif getPointValidOutsideFOVFlag(pg, point)==0:
     setPointValidOutsideFOVFlag(pg, point, 1)