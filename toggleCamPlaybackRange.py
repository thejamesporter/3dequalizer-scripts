# -*- coding: utf-8 -*-
# 3DE4.script.name:    Toggle Cam Playback Range
# 3DE4.script.version: v2.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: Sets the *end* PB range to the current frame.
#
#James Porter
#
# v2
# 19 October 2014

import tde4

c	= tde4.getCurrentCamera()
f   = tde4.getCameraNoFrames(c)
cf  = tde4.getCurrentFrame(c)
pbr = getCameraPlaybackRange(c)

try:
    pb_mark
except:
    pb_mark = 0


if (pb_mark == 0):
    tde4.setCameraPlaybackRange(c, cf, pbr[1])
    pb_mark = 1

elif (pb_mark == 1):
    tde4.setCameraPlaybackRange(c, pbr[0], cf)
    pb_mark = 2

else:
    tde4.setCameraPlaybackRange(c, 1, f)
    pb_mark = 0
