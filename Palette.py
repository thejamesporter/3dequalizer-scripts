# 3DE4.script.name:    Set 3D model colour (aka Palette)
# 3DE4.script.version: v1
# 3DE4.script.gui.button:	Orientation Controls::Palette, align-bottom-left, 70, 20
# 3DE4.script.gui.button:	Lineup Controls::Palette, align-bottom-right, 70, 20
# 3DE4.script.gui:    Main Window:: James::Set
# 3DE4.script.comment: Gui for changing colours of 3D models within the current point group.
# 3DE4.script.gui.config_menus: true

'''
James Porter. January 2017.

This is a basic gui giving several basic options to colour all or selected 3D models within a point group.

'''

import os
import sys
import tde4


colourDict      = {0:"RED", 1:"GRN", 2:"BLU", 3:"YLW", 4:"ORG",5:"LRD", 6:"LGN", 7:"LBE", 8:"PRP", 9:"CYN", 10:"BLK", 11:"GRY"}
#This is a poor man's colour dictionary. But I kinda like it like this!

#      R    G    B    Y      ORG    LR      LG      LB    Pur     Cya     Bla    Gry
R1  = [1   ,0,   0,   1,     1  ,   0.9,    0.4,   0.4,    1,      0,     0,     0.5]
G1  = [0   ,1,   0,   1,     0.3,   0.4,    0.9,   0.4,    0,      1,     0,     0.5]
B1  = [0   ,0,   1,   0,     0,     0.4,    0.4,   0.9,    1,      1,     0,     0.5]

R2  = [0.8 ,0,   0,   0.8,   0.8,   0.8  ,  0.3,   0.3,    0.7,    0,     0.1,   0.7]
G2  = [0   ,0.8, 0,   0.8,   0.3,   0.3,    0.8  , 0.3,    0,      0.7,   0.1,   0.7]
B2  = [0   ,0,   0.8, 0,     0,     0.3,    0.3,   0.8,    0.7,    0.7,   0.1,   0.7]

R3  = [0.6 ,0,   0,   0.6,   0.6,   0.6  ,  0.2,   0.2,    0.5,    0,     0.3,   0.8]
G3  = [0   ,0.6, 0,   0.6,   0.3,   0.2,    0.6  , 0.2,    0,      0.5,   0.3,   0.8]
B3  = [0   ,0,   0.6, 0,     0,     0.2,    0.2,   0.6,    0.5,    0.5,   0.3,   0.8]

R4  = [0.4 ,0,   0,   0.4,   0.4,   0.4  ,  0.1,   0.1,    0.3,    0,     0.5,   0.9]
G4  = [0   ,0.4, 0,   0.4,   0.2,   0.1,    0.4  , 0.1,    0,      0.3,   0.5,   0.9]
B4  = [0   ,0,   0.4, 0,     0,     0.1,    0.1,   0.4,    0.3,    0.3,   0.5,   0.9]

R5  = [0.2 ,0,   0,   0.2,   0.2,   0.2  ,  0.0,   0.0,    0.1,    0,     0.6,   1]
G5  = [0   ,0.2, 0,   0.2,   0.1,   0.0,    0.2  , 0.0,    0,      0.1,   0.6,   1]
B5  = [0   ,0,   0.2, 0,     0,     0.0,    0.0,   0.2,    0.1,    0.1,   0.6,   1]

button_y_offset = [0,30,50,70,90,110,130,150,170,190,210,230] #Trying long-ways

def apply_to_3d_model(model_requester,widget,action):

    pg    = tde4.getCurrentPGroup()
    apply_to_all_or_selected = tde4.getWidgetValue(model_requester,"apply_to_all")

    colour = widget[4:]
    r      = colour.split(',')[0]
    g      = colour.split(',')[1]
    b      = colour.split(',')[2]
    alpha = tde4.getWidgetValue(model_requester,"alpha_value")

    #print r,g,b,alpha

    if tde4.getWidgetValue(model_requester,"apply_to_all")==True:
        apply_to_all_or_selected = 0
    else:
        apply_to_all_or_selected = 1
    #Reversing the boolean

    models = tde4.get3DModelList(pg,apply_to_all_or_selected)

    for m in models:
        tde4.set3DModelColor(pg,m,float(r),float(g),float(b),alpha)

def apply_current_with_alpha(alpha_requester,widget,action):

    pg    = tde4.getCurrentPGroup()
    alpha = tde4.getWidgetValue(alpha_requester,"alpha_value")

    if tde4.getWidgetValue(alpha_requester,"apply_to_all")==True:
        apply_to_all_or_selected = 0
    else:
        apply_to_all_or_selected = 1
    #Reversing the boolean

    models = tde4.get3DModelList(pg,apply_to_all_or_selected)

    #2. Work out current colour of all/selected
    for m in models:
        colours = tde4.get3DModelColor(pg,m)
        r=colours[0]
        g=colours[1]
        b=colours[2]
        tde4.set3DModelColor(pg,m,r,g,b,alpha)

def tde4Gui():

    position = 10
    width    = 15
    button_x_offset = 20
    name=""

    #interface time!!
    main_requester= tde4.createCustomRequester()

    '''
    There should be a way I can subs out R1 with R2 and RED-1 with RED-0.8 etc.

    For now, 5 loops.

    '''
    for i in colourDict:
        button_x_offset = -20

        name = colourDict[i]+"-"+str(R1[i])+","+str(G1[i])+","+str(B1[i]) #much easier to read this in the requester name
        tde4.addButtonWidget(main_requester,name,"",width,position)
        tde4.setWidgetBGColor(main_requester,name,R1[i],G1[i],B1[i])
        tde4.setWidgetCallbackFunction(main_requester,name,"apply_to_3d_model")
        if name != "RED-1,0,0":
            tde4.setWidgetOffsets(main_requester,name,button_y_offset[i],0,button_x_offset,0)

    for i in colourDict:
        button_x_offset = -20

        name = colourDict[i]+"-"+str(R2[i])+","+str(G2[i])+","+str(B2[i])
        tde4.addButtonWidget(main_requester,name,"",width,position)
        tde4.setWidgetBGColor(main_requester,name,R2[i],G2[i],B2[i])
        tde4.setWidgetCallbackFunction(main_requester,name,"apply_to_3d_model")
        if name != "RED-0.8,0,0":
            tde4.setWidgetOffsets(main_requester,name,button_y_offset[i],0,button_x_offset,0)

    for i in colourDict:
        #button_x_offset = -20

        name = colourDict[i]+"-"+str(R3[i])+","+str(G3[i])+","+str(B3[i])
        tde4.addButtonWidget(main_requester,name,"",width,position)
        tde4.setWidgetBGColor(main_requester,name,R3[i],G3[i],B3[i])
        tde4.setWidgetCallbackFunction(main_requester,name,"apply_to_3d_model")
        if name != "RED-0.6,0,0":
            tde4.setWidgetOffsets(main_requester,name,button_y_offset[i],0,button_x_offset,0)


    for i in colourDict:
        button_x_offset = -20

        name = colourDict[i]+"-"+str(R4[i])+","+str(G4[i])+","+str(B4[i])
        tde4.addButtonWidget(main_requester,name,"",width,position)
        tde4.setWidgetBGColor(main_requester,name,R4[i],G4[i],B4[i])
        tde4.setWidgetCallbackFunction(main_requester,name,"apply_to_3d_model")
        if name != "RED-0.4,0,0":
            tde4.setWidgetOffsets(main_requester,name,button_y_offset[i],0,button_x_offset,0)

    for i in colourDict:
        button_x_offset = -20

        name = colourDict[i]+"-"+str(R5[i])+","+str(G5[i])+","+str(B5[i])
        tde4.addButtonWidget(main_requester,name,"",width,position)
        tde4.setWidgetBGColor(main_requester,name,R5[i],G5[i],B5[i])
        tde4.setWidgetCallbackFunction(main_requester,name,"apply_to_3d_model")
        if name != "RED-0.2,0,0":
            tde4.setWidgetOffsets(main_requester,name,button_y_offset[i],0,button_x_offset,0)

    tde4.addScaleWidget(main_requester,"alpha_value","Alpha","DOUBLE",0,1.00,0.3)
    tde4.addToggleWidget(main_requester,"apply_to_all","Apply to all/selected (on/off)",1)
    tde4.setWidgetOffsets(main_requester,"apply_to_all",90,0,5,0)
    tde4.setWidgetCallbackFunction(main_requester,"alpha_value","apply_current_with_alpha")
    tde4.postCustomRequesterAndContinue(main_requester, 'Palette',255,190)



tde4Gui()
