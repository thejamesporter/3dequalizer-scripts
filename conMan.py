# 3DE4.script.name:    conMan
# 3DE4.script.version: v1 -  June 2017
# 3DE4.script.gui:    Main Window:: James
# 3DE4.script.gui.button:    Orientation Controls::conMan(dev(, align-bottom-left, 70, 20
# 3DE4.script.gui.button:    Manual Tracking Controls::conMan(dev), align-bottom-left, 80, 20
# 3DE4.script.gui.button:    Lineup Controls::conMan(dev), align-bottom-left, 80, 20
# 3DE4.script.comment: Contraint Manager. A faster way to view and change camera constraints and modes
# 3DE4.script.gui.config_menus: true

import tde4
import os
import itertools
import operator
from collections import Counter

tde4.clearConsole()

class Return_data():
    cam_type                         = { 0: "SEQUENCE", 1: "REF_FRAME" }
    def camera_id(self,selected_camera_names):
        cam_id = []
        for cam in selected_camera_names:
            cam_id.append(tde4.findCameraByName(cam))
        return cam_id

    def listCameras(self):
        #Return a list of "SEQ" and "REF" cameras
        camera_name=[]
        camera_list = tde4.getCameraList(0)
        for c in camera_list:
            camera_name.append(tde4.getCameraName(c))
        return camera_list, camera_name

    def listLens(self):
        lens_name_list= []
        lens_id_list = tde4.getLensList()

        for Iid in lens_id_list:
            lens_name_list.append(tde4.getLensName(Iid))
        return lens_id_list,lens_name_list

    def listPG(self):
        #return point group names and types
        pg_name_list= []
        pg_id_list      = tde4.getPGroupList()

        for pid in pg_id_list:
            pg_name_list.append(tde4.getPGroupName(pid))
        return pg_id_list,pg_name_list

    def listModels(self,pg):
        model_list_name = []
        model_list_name = tde4.get3DModelList(pg,False)

    def model_path(self):
        try:
            import tde4weta #this is the only way to determine if we're at work or not.
            FILM = os.environ['FILM']
            TREE = os.environ['TREE']
            SCENE= os.environ['SCENE']
            SHOT = os.environ['SHOT']

            model_default_path = '/%s/%s/%s/%s/cam/working/3de' %(FILM, TREE, SCENE, SHOT)
        except:
            model_default_path = '/'

        return model_default_path

    def camera_constraints(self):

        cam_constraint_names         = ["self.cam_constraints_none","self.cam_constraints_fixed","self.cam_constraints_plane","self.cam_constraints_line","self.cam_constraints_circle"]
        cam_constraint_types	     = ["PCONSTR_NONE","PCONSTR_FIXED_CAMERA_POSITION","PCONSTR_PLANE","PCONSTR_LINE","PCONSTR_CIRCLE"]
        cam_constraint_type_children = ["self.constraint_orientation_undefined","self.constraint_orientation_XY","self.constraint_orientation_XZ","self.constraint_orientation_YZ"]
        cam_command = tde4.setCameraPosConstraintMode

        return cam_constraint_names, cam_constraint_types, cam_constraint_type_children, cam_command

    def camera_orients(self):

        cam_orient_names               = ["self.constraint_orientation_undefined","self.constraint_orientation_XY","self.constraint_orientation_XZ","self.constraint_orientation_YZ"]
        cam_orient_types               = ["PCONSTR_ORIENT_NONE","PCONSTR_ORIENT_XY","PCONSTR_ORIENT_XZ","PCONSTR_ORIENT_YZ"]
        cam_orient_type_children       = []
        cam_command = tde4.setCameraPosConstraintOrientation
        return cam_orient_names, cam_orient_types, cam_orient_type_children, cam_command

    def camera_sync(self):
        cam_sync_names                 =  ["self.sync_camera_none","self.sync_camera_primary","self.sync_camera_secondary"]
        cam_sync_type   	       =  ["SYNC_OFF", "SYNC_PRIMARY", "SYNC_SECONDARY"]
        cam_sync_type_children         =  ["self.sync_const_none","self.sync_const_pos_fixed","self.sync_const_pos_rot_fixed","self.sync_adjust_timeshift","self.timeshift"]
        cam_command = tde4.setCameraSyncMode
        return cam_sync_names, cam_sync_type, cam_sync_type_children, cam_command

    def cam_sync_const(self):
        cam_sync_constraints_name   = ["self.sync_const_none","self.sync_const_pos_fixed","self.sync_const_pos_rot_fixed"]
        cam_sync_constraints	    = ["SYNC_CONSTR_NONE", "SYNC_CONSTR_POS_FIXED", "SYNC_CONSTR_ROT_POS_FIXED"]
        cam_sync_children           = []
        cam_comand = tde4.setCameraSyncConstraints

        return cam_sync_constraints_name, cam_sync_constraints, cam_sync_children, cam_comand

    def cam_stereo(self):
        cam_stereo_type_name     = ["self.stereo_cam_none","self.stereo_cam_primary","self.stereo_cam_secondary"]
        cam_stereo_type          = ["STEREO_OFF","STEREO_PRIMARY","STEREO_SECONDARY"]
        cam_stereo_type_children = ["self.stereo_cam_left_eye","self.stereo_cam_right_eye","self.io_user","self.io_dynamic","self.io_adjust"]
        cam_command =tde4.setCameraStereoMode
        return cam_stereo_type_name, cam_stereo_type, cam_stereo_type_children, cam_command

    def cam_secondary_stereo(self):
        cam_stereo_secondary_type_name     = ["self.stereo_cam_secondary"]
        cam_stereo_secondary_type          = ["STEREO_SECONDARY"]
        cam_stereo_secondary_type_children = ["self.io_user","self.io_dynamic","self.io_adjust"]
        cam_command =tde4.setCameraStereoMode
        return cam_stereo_secondary_type_name, cam_stereo_secondary_type, cam_stereo_secondary_type_children, cam_command

    def cam_stereo_eye(self):
        cam_stereo_eye_name     = ["self.stereo_cam_left_eye","self.stereo_cam_right_eye"]
        cam_stereo_eye_type     = ["STEREO_LEFT","STEREO_RIGHT"]
        cam_stereo_eye_children = []
        cam_command =tde4.setCameraStereoOrientation
        return cam_stereo_eye_name, cam_stereo_eye_type, cam_stereo_eye_children, cam_command

    def cam_stereo_io(self):
        cam_stereo_io_name      = ["self.io_user","self.io_dynamic","self.io_adjust"]
        cam_stereo_io_type      = [ "STEREO_IO_STATIC","STEREO_IO_DYNAMIC","ADJUST_CAMERA_STEREO_INTEROCULAR"]
        cam_stereo_io_children  = []
        cam_command =tde4.setCameraStereoInterocular

        return cam_stereo_io_name, cam_stereo_io_type, cam_stereo_io_children, cam_command


    def pg_type(self):
        #PROBABLY WON'T BE USING THIS FOR A WHILE.
        pg_type_name       = ["self.pg_object","self.pg_mocap","self.pg_camera"]
        pg_type            = ["OBJECT","MOCAP","CAMERA"]
        pg_command =tde4.getPGroupType

        return pg_type_name,pg_type,pg_command

    def pg_postfilter(self):
        pg_smoothing_name    = ["self.pg_smoothing_off","self.pg_smoothing","self.pg_smoothing_fourier","self.pg_smoothing_position"]
        pg_smoothing_type    = ['POSTFILTER_OFF','POSTFILTER_SMOOTHING','POSTFILTER_SMOOTHING_FOURIER']
        pg_smoothing_command = tde4.setPGroupPostfilterMode

        return pg_smoothing_name,pg_smoothing_type,pg_smoothing_command

    def parent_list(self):
        parent_widgets = ["self.sync_const_none","self.sync_camera_none","self.stereo_cam_none","self.stereo_cam_secondary"]

        _,_,cam_constraint_children, _      = self.camera_constraints()
        _,_,cam_sync_children,_             = self.camera_sync()
        _,_,cam_stereo_children, _          = self.cam_stereo()
        _,_,cam_secondary_stereo_children,_ = self.cam_secondary_stereo()

        cam_constraint_children_dict    = {"self.sync_const_none": cam_constraint_children}
        cam_sync_children_dict          = {"self.sync_camera_none": cam_sync_children}
        cam_stereo_children_dict        = {"self.stereo_cam_none": cam_stereo_children}
        cam_secondary_st_dict           = {"self.stereo_cam_secondary": cam_secondary_stereo_children}


        parent_child_dicts               =[cam_constraint_children_dict,cam_sync_children_dict,cam_stereo_children_dict,cam_secondary_st_dict]
        return parent_child_dicts

    def camera_channels(self):
        camera_channels_list = ["self.transX","self.transY","self.transZ","self.rotX","self.rotY","self.rotZ"]
        return camera_channels_list

    def gui_widget_visible(self):
        widget_list           = ['self.cam_enabled','self.del_camera','self.adjust_focal','self.adjust_dist','self.lens_list', 'self.transX', 'self.transY', 'self.transZ',
                                'self.lockTrans', 'self.rotX', 'self.rotY', 'self.rotZ', 'self.lockRots', 'self.cam_constraints_none',
                                'self.cam_constraints_fixed', 'self.cam_constraints_plane', 'self.cam_constraints_line',
                                'self.cam_constraints_circle', 'self.constraint_orientation_undefined', 'self.constraint_orientation_XY',
                                'self.constraint_orientation_XZ', 'self.constraint_orientation_YZ', 'self.sync_camera_none',
                                'self.sync_camera_primary', 'self.sync_camera_secondary', 'self.sync_const_none', 'self.sync_const_pos_fixed',
                                'self.sync_const_pos_rot_fixed', 'self.sync_adjust_timeshift', 'self.timeshift', 'self.stereo_cam_none',
                                'self.stereo_cam_primary', 'self.stereo_cam_secondary', 'self.stereo_cam_left_eye', 'self.stereo_cam_right_eye',
                                'self.io_adjust', 'self.io_dynamic', 'self.io_user', 'self.io_user_input']

        return widget_list

    def gui_pg_model_widget_visible(self):
        widget_list             =['self.pg_enabled','self.del_pg','self.pg_camera','self.pg_object','self.pg_mocap','self.pg_smoothing_off',
                                  'self.pg_smoothing','self.pg_smoothing_fourier','self.pg_smoothing_position','self.mod_list',
                                  'self.add_model','self.del_model','self.colour_model','self.select_all_models']


        return widget_list

    def gui_widget_return(self):

        cam_constraint_names, cam_constraint_types, _,_     = self.camera_constraints()
        cam_orient_names, cam_orient_types, _,_             = self.camera_orients()
        cam_sync_names, cam_sync_type, _,_                  = self.camera_sync()
        cam_sync_constraints_name, cam_sync_constraints,_,_ = self.cam_sync_const()
        cam_stereo_type_name, cam_stereo_type, _,_          = self.cam_stereo()
        cam_stereo_eye_name, cam_stereo_eye_type, _,_       = self.cam_stereo_eye()
        cam_stereo_io_name, cam_stereo_io_type, _,_         = self.cam_stereo_io()


        cam_constraints_dic     = dict(zip(cam_constraint_types, cam_constraint_names))
        cam_orients_dic         = dict(zip(cam_orient_types, cam_orient_names))
        cam_sync_dic            = dict(zip(cam_sync_type, cam_sync_names))
        cam_sync_const_dic      = dict(zip(cam_sync_constraints, cam_sync_constraints_name))
        cam_stereo_dic          = dict(zip(cam_stereo_type, cam_stereo_type_name))
        cam_stereo_eye_dic      = dict(zip(cam_stereo_eye_type, cam_stereo_eye_name))
        cam_stereo_io_dic       = dict(zip(cam_stereo_io_type, cam_stereo_io_name))

        gui_widget_return_dic   = [cam_constraints_dic,
                                   cam_orients_dic,
                                   cam_sync_dic,
                                   cam_sync_const_dic,
                                   cam_stereo_dic,
                                   cam_stereo_eye_dic,
                                   cam_stereo_io_dic]

        return gui_widget_return_dic

    def gui_widget_list(self):
        #Every widget in the gui. This will allow me to (hopefully) set the layout constraints. However they work!
        #TODO: ADD POSFILTER WIDGETS!

        widget_list           = ['self.camera_list','self.add_camera','self.del_camera','self.cam_enabled', 'self.adjust_focal','self.adjust_dist',
                                 'self.lens_list', 'self.transX', 'self.transY', 'self.transZ','self.lockTrans', 'self.rotX', 'self.rotY', 'self.rotZ',
                                 'self.lockRots', 'self.cam_constraints_none', 'self.cam_constraints_fixed', 'self.cam_constraints_plane',
                                 'self.cam_constraints_line', 'self.cam_constraints_circle', 'self.constraint_orientation_undefined',
                                 'self.constraint_orientation_XY', 'self.constraint_orientation_XZ', 'self.constraint_orientation_YZ',
                                 'self.sync_camera_none', 'self.sync_camera_primary', 'self.sync_camera_secondary', 'self.sync_const_none',
                                 'self.sync_const_pos_fixed', 'self.sync_const_pos_rot_fixed', 'self.sync_adjust_timeshift', 'self.timeshift',
                                 'self.stereo_cam_none', 'self.stereo_cam_primary', 'self.stereo_cam_secondary', 'self.stereo_cam_left_eye',
                                 'self.stereo_cam_right_eye', 'self.io_adjust', 'self.io_dynamic', 'self.io_user', 'self.io_user_input',
                                 'self.pg_list','self.add_pg','self.del_pg','self.pg_enabled','self.pg_camera','self.pg_object','self.pg_mocap',
                                 'self.mod_list','self.add_model','self.del_model','self.mod_enabled','self.mod_from_pg','self.mod_from_all',
                                 'self.select_all_models','self.colour_model','self.pg_smoothing_off','self.pg_smoothing','self.pg_smoothing_fourier',
                                 'self.pg_smoothing_position']
        return widget_list

class Con_Man():

    def __init__(self):

        self.return_data                                        =  Return_data()
        self.get_camera_id_list,self.get_camera_name_list       =  self.return_data.listCameras()
        self.get_lens_id_list,self.get_lens_name_list           =  self.return_data.listLens()
        self.get_pg_id_list,self.get_pg_name_list               =  self.return_data.listPG()
        self.model_default_path                                 =  self.return_data.model_path()
        tde4.removeTimerCallback()
        self.conManGui()

    def return_dictionaries(self):
        cam_constraints_dict       = {"self.cam_constraints_none":self.return_data.camera_constraints(),"self.cam_constraints_fixed":self.return_data.camera_constraints(),"self.cam_constraints_plane":self.return_data.camera_constraints(),"self.cam_constraints_line":self.return_data.camera_constraints(),"self.cam_constraints_circle":self.return_data.camera_constraints()}
        cam_orient_dict            = {"self.constraint_orientation_undefined":self.return_data.camera_orients(),"self.constraint_orientation_XY":self.return_data.camera_orients(),"self.constraint_orientation_XZ":self.return_data.camera_orients(),"self.constraint_orientation_YZ":self.return_data.camera_orients()}
        cam_sync_dict              = {"self.sync_camera_none":self.return_data.camera_sync(),"self.sync_camera_primary":self.return_data.camera_sync(),"self.sync_camera_secondary":self.return_data.camera_sync()}
        cam_sync_const_dict        = {"self.sync_const_none":self.return_data.cam_sync_const(),"self.sync_const_pos_fixed":self.return_data.cam_sync_const(),"self.sync_const_pos_rot_fixed":self.return_data.cam_sync_const()}
        cam_stereo_dict            = {"self.stereo_cam_none": self.return_data.cam_stereo(),"self.stereo_cam_primary": self.return_data.cam_stereo(),"self.stereo_cam_secondary": self.return_data.cam_stereo()}
        cam_stereo_eye_dict        = {"self.stereo_cam_left_eye":self.return_data.cam_stereo_eye(),"self.stereo_cam_right_eye":self.return_data.cam_stereo_eye()}
        cam_stereo_io_dict         = {"self.io_user":self.return_data.cam_stereo_io(),"self.io_dynamic":self.return_data.cam_stereo_io()}

        return cam_constraints_dict, cam_orient_dict, cam_sync_dict, cam_sync_const_dict, cam_stereo_dict, cam_stereo_eye_dict, cam_stereo_io_dict


    def conManGui(self):
        #TODO: setWidgetAttachModes horseshit!

        self.con_man_requester  = tde4.createCustomRequester()

        #Camera
        tde4.addListWidget(self.con_man_requester,"self.camera_list","Cameras:",1,120)

        tde4.setWidgetOffsets(self.con_man_requester, "self.camera_list", 18, 0, 10,0)

        tde4.addButtonWidget(self.con_man_requester,"self.add_camera","+")
        tde4.setWidgetSize(self.con_man_requester, "self.add_camera", 22, 22)
        tde4.setWidgetOffsets(self.con_man_requester, "self.add_camera", 12, 50, -50, 0)

        tde4.addButtonWidget(self.con_man_requester,"self.del_camera","-")
        tde4.setWidgetSize(self.con_man_requester, "self.del_camera", 22, 22)
        tde4.setWidgetOffsets(self.con_man_requester, "self.del_camera", 12, 0,7, 0)

        tde4.addButtonWidget(self.con_man_requester,"self.select_all_cameras","All")
        tde4.setWidgetSize(self.con_man_requester, "self.select_all_cameras", 38, 22)
        tde4.setWidgetOffsets(self.con_man_requester, "self.select_all_cameras", 2, 0, -51, 0)

        tde4.addButtonWidget(self.con_man_requester,"self.select_no_cameras","Scan") #This now refreshes the list
        tde4.setWidgetSize(self.con_man_requester, "self.select_no_cameras", 38, 22)
        tde4.setWidgetOffsets(self.con_man_requester, "self.select_no_cameras",2, 0, 7, 0)

        tde4.addToggleWidget(self.con_man_requester, "self.cam_enabled", "Enabled", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.cam_enabled", 18, 0, 5, 0)


        tde4.addToggleWidget(self.con_man_requester, "self.adjust_focal", "Adjust Focal Length", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.adjust_focal", 61, 0, -20, 0)

        tde4.addToggleWidget(self.con_man_requester, "self.adjust_dist", "Adjust Distortion", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.adjust_dist", 94, 0, -20, 0)

        tde4.addSeparatorWidget(self.con_man_requester,"self.spacer1")
        #Lenses
        tde4.addListWidget(self.con_man_requester,"self.lens_list","Lenses:",0,100)
        tde4.setWidgetOffsets(self.con_man_requester, "self.lens_list", 18, 0, 0,30)

        tde4.addSeparatorWidget(self.con_man_requester,"self.spacer1")
        tde4.setWidgetOffsets(self.con_man_requester, "self.spacer1", 0, 0, -2,0)

        #Cam Constraints
        tde4.addToggleWidget(self.con_man_requester, "self.cam_constraints_none", "Contraints:   None", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.cam_constraints_fixed", "Fixed", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.cam_constraints_plane", "Plane", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.cam_constraints_line", "Line", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.cam_constraints_circle", "Circle", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.cam_constraints_fixed", 46, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.cam_constraints_plane", 62, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.cam_constraints_line", 77, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.cam_constraints_circle", 95, 0, -20, 0)
        tde4.addToggleWidget(self.con_man_requester, "self.constraint_orientation_undefined", "Undefined", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.constraint_orientation_XY", "Orient XY", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.constraint_orientation_XZ", "Orient XZ", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.constraint_orientation_YZ", "Orient YZ", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.constraint_orientation_XY", 52, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.constraint_orientation_XZ", 73,0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.constraint_orientation_YZ", 95, 0, -20, 0)

        tde4.addToggleWidget(self.con_man_requester, "self.transX", "Lock Translates:   X", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.transY", "Y", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.transZ", "Z", 0)
        tde4.addButtonWidget(self.con_man_requester,"self.lockTrans","Lock/Unlock")
        tde4.addToggleWidget(self.con_man_requester, "self.rotX", "Lock Rotates:   X", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.rotY", "Y", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.rotZ", "Z", 0)
        tde4.addButtonWidget(self.con_man_requester,"self.lockRots","Lock/Unlock")
        tde4.setWidgetOffsets(self.con_man_requester, "self.transX", 52, 0, 10, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.transY", 63, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.transZ", 73, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.lockTrans", 81, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.rotX", 52, 0, 10, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.rotY", 63, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.rotZ", 73, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.lockRots", 81, 0, -20, 0)

        tde4.addSeparatorWidget(self.con_man_requester,"self.spacer2")
        tde4.setWidgetOffsets(self.con_man_requester, "self.spacer2", 0, 0, -3, 0)

        #Sync
        tde4.addToggleWidget(self.con_man_requester, "self.sync_camera_none", "Not Sync Camera", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.sync_camera_primary", "Primary Sync", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.sync_camera_secondary", "Secondary Sync", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.sync_camera_primary", 60, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.sync_camera_secondary", 95, 0, -20, 0)
        tde4.addToggleWidget(self.con_man_requester, "self.sync_const_none", "No constraint", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.sync_const_pos_fixed", "Pos Fixed", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.sync_const_pos_rot_fixed", "Pos & Rot Fixed", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.sync_const_pos_fixed", 60, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.sync_const_pos_rot_fixed", 95, 0, -20, 0)
#         tde4.addToggleWidget(self.con_man_requester, "self.sync_mirror_points", "Mirror Points", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.sync_adjust_timeshift", "Adjust timeshift", 0)
#         tde4.setWidgetOffsets(self.con_man_requester, "self.sync_adjust_timeshift", 60, 0, 5, 0) #Probably won't move this as they'll be no mirror points
        tde4.addTextFieldWidget(self.con_man_requester,"self.timeshift","Timeshift: ")
        tde4.setWidgetOffsets(self.con_man_requester, "self.timeshift", 60, 0, -20, 20)

        tde4.addSeparatorWidget(self.con_man_requester,"self.spacer3")
#         tde4.setWidgetOffsets(self.con_man_requester, "self.spacer3", 0, 0, -1, 0)

        #Stereo
        tde4.addToggleWidget(self.con_man_requester, "self.stereo_cam_none", "Not a Stereo Cam", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.stereo_cam_primary", "Primary", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.stereo_cam_secondary", "Secondary", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.stereo_cam_none", 30, 0, -5, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.stereo_cam_primary", 60, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.stereo_cam_secondary", 95, 0, -20, 0)
        #Stereo eye
        tde4.addToggleWidget(self.con_man_requester, "self.stereo_cam_left_eye", "Left Eye", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.stereo_cam_right_eye", "Right Eye", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.stereo_cam_left_eye", 60, 0, 4, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.stereo_cam_right_eye", 95, 0, -20, 0)
        #Stereo adjustment
        tde4.addToggleWidget(self.con_man_requester, "self.io_adjust", "Adjust I/O", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.io_dynamic", "Dynamic I/O ", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.io_user", "User defined I/O", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.io_dynamic", 60, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.io_user", 95, 0, -20, 0)
        tde4.addTextFieldWidget(self.con_man_requester,"self.io_user_input"," ")
        tde4.setWidgetOffsets(self.con_man_requester, "self.io_user_input", 69, 5, 2, 0)

        tde4.addSeparatorWidget(self.con_man_requester,"self.spacer4")
        tde4.setWidgetOffsets(self.con_man_requester, "self.spacer4", 0, 0, -5, 0)
        #PGroup
        tde4.addListWidget(self.con_man_requester,"self.pg_list","PGroups:",1,70)
        tde4.addButtonWidget(self.con_man_requester,"self.add_pg","+")
        tde4.addButtonWidget(self.con_man_requester,"self.del_pg","-")
        tde4.addToggleWidget(self.con_man_requester, "self.pg_enabled", "Enabled", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.pg_camera", "Camera", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.pg_object", "Object", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.pg_mocap", "Mocap", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.pg_list", 20, 0, -2,0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.add_pg", 12, 50, -48, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.del_pg", 12, 0,5, 0)
        tde4.setWidgetSize(self.con_man_requester, "self.add_pg", 22, 22)
        tde4.setWidgetSize(self.con_man_requester, "self.del_pg", 22, 22)
        tde4.setWidgetOffsets(self.con_man_requester, "self.pg_enabled", 20, 0, 4, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.pg_camera", 60, 0, -20, 0)    #THIS DOESN'T EXIST IN 3DE'S PYTHON LIBS!!!
        tde4.setWidgetOffsets(self.con_man_requester, "self.pg_object", 77, 0, -20, 0)    #THIS DOESN'T EXIST IN 3DE'S PYTHON LIBS!!!
        tde4.setWidgetOffsets(self.con_man_requester, "self.pg_mocap", 95, 0, -20, 0)     #THIS DOESN'T EXIST IN 3DE'S PYTHON LIBS!!!
        tde4.addToggleWidget(self.con_man_requester, "self.pg_smoothing_off", "Postfilter Mode:   Off", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.pg_smoothing", "Smoothing", 0)
        tde4.addToggleWidget(self.con_man_requester, "self.pg_smoothing_fourier", "Smoothing-Fourier", 0)
        tde4.addScaleWidget(self.con_man_requester, "self.pg_smoothing_position","Position","DOUBLE",0,50,2.50) #THIS DOESN'T EXIST IN 3DE'S PYTHON LIBS!!!
        tde4.setWidgetOffsets(self.con_man_requester, "self.pg_smoothing_off", 35, 0, 5, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.pg_smoothing", 60, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.pg_smoothing_fourier", 95, 0, -20, 0)


        tde4.addSeparatorWidget(self.con_man_requester,"self.spacer5")
        tde4.setWidgetOffsets(self.con_man_requester, "self.spacer5", 0, 0, -5, 0)

        tde4.addListWidget(self.con_man_requester,"self.mod_list","3D Models:",1,80)
        tde4.setWidgetOffsets(self.con_man_requester, "self.mod_list", 20, 0, 0,0)
        tde4.addToggleWidget(self.con_man_requester, "self.mod_enabled", "Visible", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.mod_enabled", 20, 0, 2, 0)
        tde4.addToggleWidget(self.con_man_requester, "self.mod_from_pg", "From Single PG", 1)
        tde4.addToggleWidget(self.con_man_requester, "self.mod_from_all", "From selected PGs", 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.mod_from_pg", 60, 0, -20, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.mod_from_all", 95, 0, -20, 0)
        tde4.addButtonWidget(self.con_man_requester,"self.add_model","+")
        tde4.addButtonWidget(self.con_man_requester,"self.del_model","-")
        tde4.addButtonWidget(self.con_man_requester,"self.select_all_models","All")
        tde4.addButtonWidget(self.con_man_requester,"self.colour_model","Color")
        tde4.setWidgetOffsets(self.con_man_requester, "self.add_model", 12, 50, -80, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.del_model", 12, 0, 7, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.select_all_models", 2, 0,-51, 0)
        tde4.setWidgetOffsets(self.con_man_requester, "self.colour_model", 2, 0,7, 0)
        tde4.setWidgetSize(self.con_man_requester, "self.add_model", 22, 22)
        tde4.setWidgetSize(self.con_man_requester, "self.del_model", 22, 22)
        tde4.setWidgetSize(self.con_man_requester, "self.select_all_models", 40, 22)
        tde4.setWidgetSize(self.con_man_requester, "self.colour_model", 40, 22)


        self.refresh_lists('con_man_requester','self.camera_list','')           #The first time filling out the list...

        #setWidgetAttachModes This is for those who have silly widget sizes.
#         self.widget_attach_modes = self.return_data.gui_widget_list()
#
#         for wam in self.widget_attach_modes:
#             tde4.setWidgetAttachModes(self.con_man_requester,wam,"ATTACH_POSITION","ATTACH_POSITION","ATTACH_AS_IS","ATTACH_AS_IS")

        locks = ['self.transX','self.transY','self.transZ','self.lockTrans','self.rotX','self.rotY','self.rotZ','self.lockRots']
        for l in locks:
            tde4.setWidgetCallbackFunction(self.con_man_requester, l,"gui.lock_unlock")

        get_n_set =['self.cam_constraints_none','self.cam_constraints_fixed','self.cam_constraints_plane','self.cam_constraints_line','self.cam_constraints_circle',
                    'self.constraint_orientation_undefined','self.constraint_orientation_XY','self.constraint_orientation_XZ','self.constraint_orientation_YZ',
                    'self.sync_camera_none','self.sync_camera_primary','self.sync_camera_secondary','self.sync_const_none','self.sync_const_pos_fixed',
                    'self.sync_const_pos_rot_fixed','self.stereo_cam_none','self.stereo_cam_primary','self.stereo_cam_secondary','self.stereo_cam_left_eye',
                    'self.stereo_cam_right_eye','self.io_dynamic','self.io_user']

        for g in get_n_set:
            tde4.setWidgetCallbackFunction(self.con_man_requester, g,"gui.get_and_set")

        tde4.setWidgetCallbackFunction(self.con_man_requester,"self.camera_list","gui.selected_camera")
        tde4.setWidgetCallbackFunction(self.con_man_requester,"self.cam_enabled","gui.camera_enabled")
        tde4.setWidgetCallbackFunction(self.con_man_requester,"self.add_camera","gui.add_camera")
        tde4.setWidgetCallbackFunction(self.con_man_requester,"self.del_camera","gui.del_camera")
        tde4.setWidgetCallbackFunction(self.con_man_requester,"self.select_all_cameras","gui.selected_camera")
        tde4.setWidgetCallbackFunction(self.con_man_requester,"self.select_no_cameras","gui.refresh_lists")
        tde4.setWidgetCallbackFunction(self.con_man_requester,"self.adjust_focal","gui.adjust_params")
        tde4.setWidgetCallbackFunction(self.con_man_requester,"self.adjust_dist","gui.adjust_params")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.lens_list","gui.selected_lens")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.sync_adjust_timeshift","gui.adjust_timeshift")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.timeshift","gui.set_values")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.stereo_cam_right_eye","gui.get_and_set")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.io_adjust","gui.adjust_io")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.pg_list","gui.select_pg")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.pg_enabled","gui.pg_enabled")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.add_pg","gui.add_pg")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.del_pg","gui.del_pg")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.pg_smoothing_off","gui.pg_smoothing")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.pg_smoothing","gui.pg_smoothing")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.pg_smoothing_fourier","gui.pg_smoothing")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.pg_smoothing_position","gui.pg_smoothing")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.mod_list","gui.selected_model")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.select_all_models","gui.selected_model")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.mod_enabled","gui.model_enabled")
#         tde4.setWidgetCallbackFunction(self.con_man_requester, "self.pg_camer","gui.set_values")
#         tde4.setWidgetCallbackFunction(self.con_man_requester, "self.pg_object","gui.set_values")
#         tde4.setWidgetCallbackFunction(self.con_man_requester, "self.pg_mocap","gui.set_values")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.add_model","gui.add_model")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.del_model","gui.del_model")
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.mod_from_pg","gui.mod_widget_selection")  #<prob don't need
        tde4.setWidgetCallbackFunction(self.con_man_requester, "self.mod_from_all","gui.mod_widget_selection") #<prob don't need

        #The models will be off until they select the PG for now

        self.models_widgets = ["self.mod_list","self.mod_enabled","self.mod_from_pg","self.mod_from_all"]
        for mw in self.models_widgets:
            tde4.setWidgetSensitiveFlag(self.con_man_requester,mw,0)

        self.widget_values = [0]* len(self.return_data.gui_widget_visible())
        #Switch everything except the cam_list to off
        self.set_gui_sensitive_flag(self.widget_values)
        #Call back for attributes will assign it to a selected lens.

        tde4.setTimerCallbackFunction("gui.update_selected",300)

        tde4.postCustomRequesterAndContinue(self.con_man_requester,"conMan",450,850)



    def set_gui_sensitive_flag(self,widget_values):

        self.widget_list = self.return_data.gui_widget_visible()

        for i, w in enumerate(self.widget_list):
            tde4.setWidgetSensitiveFlag(self.con_man_requester,w,self.widget_values[i])

        self.pg_widget_list = self.return_data.gui_pg_model_widget_visible()

        for i, w in enumerate(self.pg_widget_list):
            tde4.setWidgetSensitiveFlag(self.con_man_requester,w,0)


#         tde4.setWidgetSensitiveFlag(self.con_man_requester,"self.mod_from_pg",0)  #This will probably always be 0!!
#         tde4.setWidgetSensitiveFlag(self.con_man_requester,"self.mod_from_all",0) #This will probably always be 0!!


    def set_gui_on_off(self,widget_values):
        self.widget_list = self.return_data.gui_widget_visible()
        for i, w in enumerate(self.widget_list):
            tde4.setWidgetValue(self.con_man_requester,w,str(self.widget_values[i]))

    def refresh_lists(self,con_man_requester,widget,action):
        tde4.removeTimerCallback()

        tde4.removeAllListWidgetItems(self.con_man_requester,"self.camera_list")
        tde4.removeAllListWidgetItems(self.con_man_requester,"self.lens_list")
        tde4.removeAllListWidgetItems(self.con_man_requester,"self.pg_list")
        tde4.removeAllListWidgetItems(self.con_man_requester,"self.mod_list")

        self.get_camera_id_list     = []
        self.get_camera_name_list   = []
        self.get_lens_id_list       = []
        self.get_lens_name_list     = []
        self.get_pg_id_list         = []
        self.get_pg_name_list       = []
        self.cam_id                 = []
        self.pg_id                  = []

        self.get_camera_id_list,self.get_camera_name_list       =  self.return_data.listCameras()
        self.get_lens_id_list,self.get_lens_name_list           =  self.return_data.listLens()
        self.get_pg_id_list,self.get_pg_name_list               =  self.return_data.listPG()
        self.get_pg_type,self.get_pg_name,_                     =  self.return_data.pg_type()

        self.widget_values = [0]* len(self.return_data.gui_widget_visible()) #This is the number of [0's] rather than fucking [0,0,0...]
        self.set_gui_sensitive_flag(self.widget_values)
        self.set_gui_on_off(self.widget_values) #Reset the lights!!

        for i, cam in enumerate(self.get_camera_id_list):
            tde4.insertListWidgetItem(self.con_man_requester,"self.camera_list",str(self.get_camera_name_list[i]),i)

        for i, lens in enumerate(self.get_lens_id_list):
            tde4.insertListWidgetItem(self.con_man_requester,"self.lens_list",str(self.get_lens_name_list[i]),i)

        for i, pg in enumerate(self.get_pg_id_list):
            tde4.insertListWidgetItem(self.con_man_requester,"self.pg_list",str(self.get_pg_name_list[i]),i)

        selected_cams = tde4.getCameraList(1)
        for i, c in enumerate(self.get_camera_id_list):
            for s in selected_cams:
                if c in s:
                    tde4.setListWidgetItemSelectionFlag(self.con_man_requester,"self.camera_list",i,1)
                    self.cam_id.append(c)

        selected_pg = tde4.getPGroupList(1)
        for i, p in enumerate(self.get_pg_id_list):
            for s in selected_pg:
                if p in s:
                    tde4.setListWidgetItemSelectionFlag(self.con_man_requester,"self.pg_list",i,1)
                    self.pg_id.append(c)



    def selected_camera(self,con_man_requester,widget,action):
        #This is the driver of the whole tool. The user has to start here etc.
        tde4.removeTimerCallback() #Hopefully, this'll stop crashing!!

        self.camera_list_widget_count = 0    #Counting number of listed cameras in the widget
        self.selected_camera_names    = []   #Each time the selection ==1, we add the name to this list
        self.cam_id                   = []   #From the selected_camera_names, we loop through to find the actual ID of a camera
        self.multiple_selected        = 0    #This is a simple counter. Every time the list widget has a selection, it adds 1 to itself
                                             #If multiple selected == 0, it means only 1 camera is selected.
        self.selected_sequence_list   = []   #Holds the cam ID of any camera marked "SEQUENCE". It also helps work out what cam is what
        self.selected_ref_list        = []   #Same as above but REF_FRAME

        self.camera_list_widget_count = tde4.getListWidgetNoItems(self.con_man_requester,"self.camera_list")

        self.parent_child_dict        = self.return_data.parent_list()
#         print self.parent_child_dict
        self.gui_list                 = self.return_data.gui_widget_visible()
#         self.gui_command_returns      = []
                                        #There's two getCamereraStereoModes to help me turn off the widgets for seconary cams
        self.gui_get_commands         = [tde4.getCameraPosConstraintMode,
                                        tde4.getCameraPosConstraintOrientation,
                                        tde4.getCameraSyncMode,
                                        tde4.getCameraSyncConstraints,
                                        tde4.getCameraStereoMode,
                                        tde4.getCameraStereoMode,
                                        tde4.getCameraStereoOrientation,
                                        tde4.getCameraStereoInterocular] #This is the return value
        self.gui_widget_return_value   =self.return_data.gui_widget_return()
        self.para_adjust_list          =['ADJUST_LENS_FOCAL_LENGTH','ADJUST_LENS_DISTORTION_PARAMETER']
        self.para_adjust_widget        =['self.adjust_focal','self.adjust_dist']
        self.widget_values = [0]* len(self.return_data.gui_widget_visible())
        self.set_gui_on_off(self.widget_values)

        self.widget_values = [1]* len(self.return_data.gui_widget_visible())

        #Turn off selection
        for c in tde4.getCameraList(0):
            tde4.setCameraSelectionFlag(c,0)


        if widget == "self.select_all_cameras":
            for c in range(self.camera_list_widget_count):
                tde4.setListWidgetItemSelectionFlag(self.con_man_requester,"self.camera_list",c,1)
                self.multiple_selected+=1
                self.selected_camera_names.append(tde4.getListWidgetItemLabel(self.con_man_requester,"self.camera_list",c))

            for i, cam in enumerate(self.selected_camera_names):
                self.cam_id.append(tde4.findCameraByName(cam))
                if tde4.getCameraType(self.cam_id[i])=="SEQUENCE":
                    self.selected_sequence_list.append(self.cam_id[i])
                else:
                    self.selected_ref_list.append(self.cam_id[i])
            for c in self.cam_id:
                tde4.setCameraSelectionFlag(c,1)

        else: # User has selected specific cameras


            for c in range(self.camera_list_widget_count):
                if tde4.getListWidgetItemSelectionFlag(self.con_man_requester,"self.camera_list",c) ==1:
                    self.multiple_selected+=1
                    self.selected_camera_names.append(tde4.getListWidgetItemLabel(self.con_man_requester,"self.camera_list",c))


            #Get the ID of the selected camera's names, make sequence and ref lists
            for i, cam in enumerate(self.selected_camera_names):
                self.cam_id.append(tde4.findCameraByName(cam))
                if tde4.getCameraType(self.cam_id[i])=="SEQUENCE":
                    self.selected_sequence_list.append(self.cam_id[i])
                else:
                    self.selected_ref_list.append(self.cam_id[i])

        #@@@@@@ 1 CAMERA SELECTED @@@@@@@@
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        if self.multiple_selected == 1: #Single cam selected. Show everything.
            tde4.setCameraSelectionFlag(self.cam_id[0],1)

            self.widget_values    = [1] * len(self.return_data.gui_widget_visible())
            self.set_gui_sensitive_flag(self.widget_values) #Run the switch method

            #Now get the relevant data
            self.get_sync_timeshift     =tde4.getCameraSyncTimeshift(self.cam_id[0])
            self.get_stereo_io          =tde4.getCameraStereoStaticInterocular(self.cam_id[0])
            tde4.setWidgetValue(self.con_man_requester,"self.timeshift",str(self.get_sync_timeshift)+ " frames")
            tde4.setWidgetValue(self.con_man_requester,"self.io_user_input",str(self.get_stereo_io) + " cm")
            tde4.setWidgetValue(self.con_man_requester,"self.cam_enabled",str(tde4.getCameraEnabledFlag(self.cam_id[0]))) #Switches on/off based if the camera is dis

            self.camera_lens_id     = tde4.getCameraLens(self.cam_id[0])
            self.camera_lens_name   = tde4.getLensName(self.camera_lens_id)
            self.lens_list_widget_count = tde4.getListWidgetNoItems(self.con_man_requester,"self.lens_list")
            #select the attached lens
            tde4.setListWidgetItemSelectionFlag(self.con_man_requester,"self.lens_list",self.get_lens_id_list.index(self.camera_lens_id),1)

            #Light up the rest of board!! Now if the get_return matches something in the "Parent List", the other's will be switched off
            for g in self.gui_get_commands:
                get_return = g(self.cam_id[0])
#                 print str(get_return)
                for rv in self.gui_widget_return_value:
                    if str(get_return) in rv:
                        par =  rv[get_return]
                        tde4.setWidgetValue(self.con_man_requester,rv[get_return],"1")
            #Turn off the kiddies!!
            for i, p in enumerate(self.parent_child_dict):
                parent = p.keys()
                #Loop through the parents key. If the parent widget is on:
                if tde4.getWidgetValue(self.con_man_requester,parent[0]) ==1:
                    #strip the key into items
                    for c, child_list in p.items():
                        #for all the items within the stripped list, turn off!
                        for i, h in enumerate(child_list):
                            tde4.setWidgetSensitiveFlag(self.con_man_requester, h,0)


            #Get the XYZs and shit
            self.return_channels  = tde4.getCameraPosRotLockChannelFlags(self.cam_id[0])
            self.camera_channels  = self.return_data.camera_channels()
            for i,r in enumerate(self.return_channels):
                tde4.setWidgetValue(self.con_man_requester,self.camera_channels[i],str(r))

            if tde4.getWidgetSensitiveFlag(self.con_man_requester,"self.io_user")!=1:
                tde4.setWidgetSensitiveFlag(self.con_man_requester,"self.io_user_input",0)
            #Finally, if there's anything in the parameter adjustment window linked to that camera

            model                          = tde4.getLensLDModel(self.camera_lens_id)
            Dmodel1                        = tde4.getLDModelParameterName(model,0)
            Dmodel2                        = tde4.getLDModelParameterName(model,3)
            para_arg                       = ['',Dmodel1,Dmodel2] #this is to check if parameter adjusts are on/off

            for i, pal in enumerate(self.para_adjust_list):
                if tde4.getParameterAdjustFlag(self.camera_lens_id,pal,para_arg[i])==1:
                    tde4.setWidgetValue(self.con_man_requester,self.para_adjust_widget[i],"1")
                else:
                    tde4.setWidgetValue(self.con_man_requester,self.para_adjust_widget[i],"0")

            if tde4.getParameterAdjustFlag(self.cam_id[0],"ADJUST_CAMERA_STEREO_INTEROCULAR","")==1:

                tde4.setWidgetValue(self.con_man_requester,"self.io_adjust","1")
                tde4.setWidgetValue(self.con_man_requester,"self.io_dynamic","0")
                tde4.setWidgetValue(self.con_man_requester,"self.io_user","0")
                tde4.setWidgetSensitiveFlag(self.con_man_requester,"self.io_user_input",0)

        #@@@@@@ MULTIPLE SELECTED @@@@@@@@@
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        elif self.multiple_selected>1:

            self.switch_off = ["self.sync_camera_primary","self.stereo_cam_left_eye","self.stereo_cam_right_eye",
                               "self.stereo_cam_primary","self.stereo_cam_secondary","self.io_adjust","self.io_user",
                               "self.io_user_input","self.io_dynamic"] #Get the index of the list in the class. The change that value to 0

            for o in self.switch_off:
                self.widget_values[self.gui_list.index(o)]=0
                self.set_gui_sensitive_flag(self.widget_values) #Run the switch method

            for c in self.cam_id:
                tde4.setCameraSelectionFlag(c,1)
#             self.multiple_selection_list = []
#             self.gui_get_multi_check = self.gui_get_commands
#             self.gui_get_multi_check.pop(self.get_gui_multi_check.index )
#             for g in self.gui_get_commands.pop():
#                 for c in self.cam_id:
#                     self.multiple_selection_list.append(g(c))
#     #
#             for m in self.multiple_selection_list:
#                 print m
#
#             d = Counter(' '.join(self.multiple_selection_list).split())
#
#             # create a list of tuples, ordered by occurrence frequency
#             sorted_d = sorted(d.items(), key=operator.itemgetter(1), reverse=True)
#
#             # print all entries that occur more than once
#             for x in sorted_d:
#                 if x[1] > 1:
#                     print(x[1], x[0])


            #@@@@@@ REF_FRAM IN SELECTION @@@@@@@@
            #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if self.selected_ref_list!=[]:
            self.switch_on     = ["self.cam_enabled","self.lens_list","self.transX","self.transY","self.transZ","self.lockTrans","self.rotX","self.rotY","self.rotZ","self.lockRots"]
            self.widget_values = [0]* len(self.return_data.gui_widget_visible())
            for o in self.switch_on:
                self.widget_values[self.gui_list.index(o)]=1

            self.set_gui_sensitive_flag(self.widget_values)
            self.return_channels  = tde4.getCameraPosRotLockChannelFlags(self.cam_id[0])
            self.camera_channels  = self.return_data.camera_channels()
            tde4.setWidgetValue(self.con_man_requester,"self.cam_enabled",str(tde4.getCameraEnabledFlag(self.cam_id[0])))
            for i,r in enumerate(self.return_channels):

                tde4.setWidgetValue(self.con_man_requester,self.camera_channels[i],str(r))
            tde4.setWidgetSensitiveFlag(self.con_man_requester,'self.del_camera',1)

        if tde4.getWidgetValue(self.con_man_requester,"self.transX")==1 and tde4.getWidgetValue(self.con_man_requester,"self.transY")==1 and tde4.getWidgetValue(self.con_man_requester,"self.transZ"):
            tde4.setWidgetLabel(self.con_man_requester,"self.lockTrans","Unlock")
        else:
            tde4.setWidgetLabel(self.con_man_requester,"self.lockTrans","Lock")

        if tde4.getWidgetValue(self.con_man_requester,"self.rotX")==1 and tde4.getWidgetValue(self.con_man_requester,"self.rotY")==1 and tde4.getWidgetValue(self.con_man_requester,"self.rotZ"):
            tde4.setWidgetLabel(self.con_man_requester,"self.lockRots","Unlock")
        else:
            tde4.setWidgetLabel(self.con_man_requester,"self.lockRots","Lock")

        tde4.setTimerCallbackFunction("gui.update_selected",300)

    def selected_lens(self,con_man_requester,widget,action):
        tde4.removeTimerCallback()
        self.lens_list_widget_count = tde4.getListWidgetNoItems(self.con_man_requester,"self.lens_list")

        for l in range(self.lens_list_widget_count):

            if tde4.getListWidgetItemSelectionFlag(self.con_man_requester,"self.lens_list",l) ==1:
                #Apply lens to selected cameras (based on returned list)
                for i, cam in enumerate(self.cam_id):
                    tde4.setCameraLens(cam,self.get_lens_id_list[l])

        tde4.setTimerCallbackFunction("gui.update_selected",300)

    def camera_enabled(self,con_man_requester,widget,action):
        for c in self.cam_id:
            tde4.setCameraEnabledFlag(c,tde4.getWidgetValue(self.con_man_requester,'self.cam_enabled'))
    def pg_enabled(self,con_man_requester,widget,action):
        for pg in self.pg_id:
            tde4.setPGroupEnabledFlag(pg,tde4.getWidgetValue(self.con_man_requester,'self.pg_enabled'))


    def pg_smoothing(self,con_man_requester,widget,action):
        pass



    def select_pg(self,con_man_requester,widget,action):
        tde4.removeTimerCallback()
        tde4.removeAllListWidgetItems(self.con_man_requester,"self.mod_list")
        self.selected_pg_names          = []
        self.pg_id                      = []
        self.pg_list_widget_count       = tde4.getListWidgetNoItems(self.con_man_requester,"self.pg_list")
        self.pg_type                    = []
        self.mod_id                     = []
        self.mod_id_flat_list           = [] #This is to strip the list of lists
        self.mod_pg_name                = []
        self.multi_mod_pg               = [] #A list of PGs if the user selects multiple PGs (for the model list)
        selection_count = 0

        self.pg_widget,self.pg_callback,command = self.return_data.pg_type()
        self.pg_smoothing_widget,self.pg_smoothing_callback,pg_command = self.return_data.pg_postfilter()

        for p in tde4.getPGroupList(0):
            tde4.setPGroupSelectionFlag(p,0)

        self.widget_list = self.return_data.gui_pg_model_widget_visible()

        for i, w in enumerate(self.widget_list):
            tde4.setWidgetSensitiveFlag(self.con_man_requester,w,self.widget_values[i])

        #These will e switched on when a selection is made!
        self.pg_mod_wigets = ["self.mod_list","self.mod_enabled","self.add_model","self.colour_model","self.select_all_models","self.mod_from_pg",
                              "self.mod_from_all","self.pg_smoothing_off","self.pg_smoothing","self.pg_smoothing_fourier",
                              "self.pg_smoothing_position","self.pg_enabled",'self.del_pg']



        for pgmw in self.pg_mod_wigets:
            tde4.setWidgetSensitiveFlag(self.con_man_requester,pgmw,1)

        for pg in range(self.pg_list_widget_count):
            if tde4.getListWidgetItemSelectionFlag(self.con_man_requester,"self.pg_list",pg) ==1:

                self.selected_pg_names.append(tde4.getListWidgetItemLabel(self.con_man_requester,"self.pg_list",pg))

        for spn in self.selected_pg_names:
            self.pg_id.append(tde4.findPGroupByName(spn))
            selection_count+=1

        #We have a list. Now we'll light up the pg type. Need to do for single/multiple selections
        if selection_count==1:
#             print "Single PG selected"
            tde4.setWidgetSensitiveFlag(self.con_man_requester,"self.add_model",1)

            for pid in self.pg_id:
                tde4.setPGroupSelectionFlag(pid,0)
                for i, call in enumerate(self.pg_callback):
                    if call == tde4.getPGroupType(pid):
                        tde4.setWidgetValue(self.con_man_requester,self.pg_widget[i],"1")
                    else:
                        tde4.setWidgetValue(self.con_man_requester,self.pg_widget[i],"0")

                for i, call in enumerate(self.pg_smoothing_callback):
                    if call == tde4.getPGroupPostfilterMode(pid):
                        if call == 'POSTFILTER_OFF':
                            tde4.setWidgetSensitiveFlag(self.con_man_requester,'self.pg_smoothing_position',0)
                        else:
                            tde4.setWidgetSensitiveFlag(self.con_man_requester,'self.pg_smoothing_position',1)
                        tde4.setWidgetValue(self.con_man_requester,self.pg_smoothing_widget[i],"1")
#                         tde4.setWidgetValue(self.con_man_requester,"self.pg_smoothing_position",float(tde4.))
                    else:
                        tde4.setWidgetValue(self.con_man_requester,self.pg_smoothing_widget[i],"0")




            #Now populate model list.
            tde4.setWidgetValue(self.con_man_requester,"self.mod_from_pg","1")
            tde4.setWidgetValue(self.con_man_requester,"self.mod_from_all","0")
            tde4.setWidgetValue(self.con_man_requester,"self.pg_enabled",str(tde4.getPGroupEnabledFlag(self.pg_id[0])))

            self.mod_id = tde4.get3DModelList(self.pg_id[0])
            self.mod_id_flat_list = self.mod_id


            for i, mod in enumerate(self.mod_id):
                tde4.insertListWidgetItem(self.con_man_requester,"self.mod_list",str(tde4.get3DModelName(self.pg_id[0],mod)),i)
                self.mod_pg_name.append (self.pg_id[0]) #Always have the same number of mod id's as the model list

        else: #Multi selection

            tde4.setWidgetSensitiveFlag(self.con_man_requester,"self.add_model",0)
            tde4.setWidgetSensitiveFlag(self.con_man_requester,"self.del_model",1)
            tde4.setWidgetValue(self.con_man_requester,"self.mod_from_pg","0")
            tde4.setWidgetValue(self.con_man_requester,"self.mod_from_all","1")

            for p, pid in enumerate(self.pg_id):
                tde4.setPGroupSelectionFlag(pid,0)
                self.mod_id.append(tde4.get3DModelList(pid))
                for m in self.mod_id:
                    for j in m:
                        self.mod_id_flat_list.append(j)
                        self.mod_pg_name.append(self.pg_id[p])
                tde4.setWidgetValue(self.con_man_requester,"self.pg_enabled",str(tde4.getPGroupEnabledFlag(pid)))


            for i,mid in enumerate(self.mod_id):
                for m in mid:
                    tde4.insertListWidgetItem(self.con_man_requester,"self.mod_list",str(tde4.get3DModelName(self.pg_id[i],m)),i)
#                     self.mod_pg_name.append(self.pg_id[i])

        tde4.setTimerCallbackFunction("gui.update_selected",300)

    def selected_model(self,con_man_requester,widget,action):
        self.model_list_widget_count = tde4.getListWidgetNoItems(self.con_man_requester,"self.mod_list")
        self.selected_model_names    = []
        self.selected_model_id       = []
        self.selected_model_pg_id    = []

        tde4.setWidgetSensitiveFlag(self.con_man_requester,'self.del_model',1)
        #number of items drives the self.mod_id_flat_list

        if widget == 'self.select_all_models':
            #All models in the list. Need to get the model id and the pg id, which should all be in self.mod_pg_name and self.mod_id
            for i in range(tde4.getListWidgetNoItems(self.con_man_requester,"self.mod_list")):
                tde4.setListWidgetItemSelectionFlag(self.con_man_requester,"self.mod_list",i,1)
                self.selected_model_id = self.mod_id_flat_list
                self.selected_model_pg_id = self.mod_pg_name


        else: # User has selected specific model
            for i in range(tde4.getListWidgetNoItems(self.con_man_requester,"self.mod_list")):
                if tde4.getListWidgetItemSelectionFlag(self.con_man_requester,"self.mod_list",i) ==1:
                    self.selected_model_id.append(self.mod_id_flat_list[i])
                    self.selected_model_pg_id.append(self.mod_pg_name[i])



    def model_enabled(self,con_man_requester,widget,action):
        pass
        #Fix this.
#         print self.selected_model_pg_id
#         print self.selected_model_id
#         print len(self.selected_model_id)

#         if tde4.getWidgetValue(self.con_man_requester,widget) == 1:
#             tde4.setWidgetValue(self.con_man_requester,widget,"1")
#             for m in xrange(len(self.selected_model_pg_id)):
#                 print m
#                 tde4.set3DModelVisibleFlag(self.selected_model_pg_id[m],self.selected_model_id[m],1)
#
#         else:
#             tde4.setWidgetValue(self.con_man_requester,widget,"0")
#             for m in xrange(len(self.selected_model_pg_id)):
#                 tde4.set3DModelVisibleFlag(self.selected_model_pg_id[m],self.selected_model_id[m],0)
#







    def lock_unlock(self,con_man_requester,widget,action):

        if widget =="self.lockTrans" or widget =="self.lockRots":
            channels_results = []
            self.enabled  = {"self.lockTrans":["self.transX","self.transY","self.transZ"],"self.lockRots":["self.rotX","self.rotY","self.rotZ"]}
            self.channels    = ["self.transX","self.transY","self.transZ","self.rotX","self.rotY","self.rotZ"]
            #Take the value for the first selected camera. That way, everything is set the same

            for n in self.channels:
                #This is not getting the t/r xyz of the camera but the widget's on off value. That way, multiple selected have the same thing applied
                channels_results.append(tde4.getWidgetValue(self.con_man_requester,n))

            if widget  == 'self.lockTrans':
                a = 0
                b = 3
                list_range = [0,1,2]
            else:
                a = 3
                b = 6
                list_range = [3,4,5]


            for h, c in enumerate(channels_results[a:b]):
                if c == 1:
                    channels_results[list_range[h]]=0
                    tde4.setWidgetLabel(self.con_man_requester,widget,"Lock")
                else:
                    channels_results[list_range[h]]=1
                    tde4.setWidgetLabel(self.con_man_requester,widget,"Unlock")


            for c in self.cam_id:
                tde4.setCameraPosRotLockChannelFlags(c,channels_results[0],channels_results[1],channels_results[2],channels_results[3],channels_results[4],channels_results[5])
                for s, e in enumerate(self.channels):
                    tde4.setWidgetValue(self.con_man_requester,e,str(channels_results[s]))

        else: #Individual channels
            self.channels = {"self.transX":0,"self.transY":1,"self.transZ":2,"self.rotX":3,"self.rotY":4,"self.rotZ":5}

            #Get which widget is selected. If it's a single channel, get the current channels and replace the selected
            for c in self.cam_id:
                self.return_channels = tde4.getCameraPosRotLockChannelFlags(c)
#                 print self.return_channels
                if str(widget) in self.channels:
                    self.return_channels[self.channels[widget]] = int(tde4.getWidgetValue(self.con_man_requester,widget))
                    tde4.setCameraPosRotLockChannelFlags(c,self.return_channels[0],self.return_channels[1],self.return_channels[2],self.return_channels[3],self.return_channels[4],self.return_channels[5])


    def adjust_params(self,con_man_requester,widget,action):
        for c in self.cam_id:
            l = tde4.getCameraLens(c)

            if widget== "self.adjust_focal":
                if tde4.getWidgetValue(self.con_man_requester,widget)==1:
                    tde4.setParameterAdjustFlag(l, "ADJUST_LENS_FOCAL_LENGTH","",1)
                else:
                    tde4.setParameterAdjustFlag(l, "ADJUST_LENS_FOCAL_LENGTH","",0)

            else:
                model = tde4.getLensLDModel(l)
                Dmodel1 = tde4.getLDModelParameterName(model,0)
                Dmodel2 = tde4.getLDModelParameterName(model,3)

                if tde4.getWidgetValue(self.con_man_requester,widget)==1:
                    tde4.setLensDynamicDistortionMode(l,"DISTORTION_STATIC")
                    tde4.setParameterAdjustFlag(l, "ADJUST_LENS_DISTORTION_PARAMETER",Dmodel1,1)
                    tde4.setParameterAdjustFlag(l, "ADJUST_LENS_DISTORTION_PARAMETER",Dmodel2,1)
                else:
                    tde4.setParameterAdjustFlag(l, "ADJUST_LENS_DISTORTION_PARAMETER",Dmodel1,0)
                    tde4.setParameterAdjustFlag(l, "ADJUST_LENS_DISTORTION_PARAMETER",Dmodel2,0)


    def adjust_io(self,con_man_requester, widget,action):

        for c in self.cam_id:
            if tde4.getWidgetValue(con_man_requester,widget) ==1:
                tde4.setParameterAdjustFlag(c,"ADJUST_CAMERA_STEREO_INTEROCULAR","",1)
                tde4.setWidgetSensitiveFlag(self.con_man_requester, "self.io_user_input",0)
                tde4.setWidgetValue(con_man_requester,"self.io_dynamic","0")
                tde4.setWidgetValue(con_man_requester,"self.io_user","0")
            else:
                tde4.setWidgetValue(con_man_requester,"self.io_user","1")
                tde4.setCameraStereoInterocular(c,"STEREO_IO_STATIC")
                tde4.setWidgetSensitiveFlag(self.con_man_requester, "self.io_user_input",1)

    def adjust_timeshift(self,con_man_requester, widget,action):
        for c in self.cam_id:
            if tde4.getWidgetValue(con_man_requester,widget) ==1:
                tde4.setParameterAdjustFlag(c,"ADJUST_CAMERA_SYNC_TIMESHIFT","",1)
            else:
                tde4.setParameterAdjustFlag(c,"ADJUST_CAMERA_SYNC_TIMESHIFT","",0)

    def set_values(self,con_man_requester,widget,action):
        #This is for text entry values
        print tde4.getWidgetValue(self.con_man_requester,widget)

        if widget == "self.timeshift":
            command = tde4.setCameraSyncTimeshift
            units = " frames"
        else:
            command = tde4.setCameraStereoStaticInterocular
            units = " cm"

        for c in self.cam_id:
            command(c,float(tde4.getWidgetValue(self.con_man_requester,widget)))

        #set value

        tde4.setWidgetValue(self.con_man_requester, widget,"%s %s" %(tde4.getWidgetValue(self.con_man_requester,widget),units))


    def get_and_set(self,con_man_requester,widget,action):
        tde4.removeTimerCallback()
    #this is the meat of the dish!

        self.get_list       = [] #this is the names eg self.cam_constraints_fixed
        self.set_list       = [] #This is the command being set
        self.get_default    = []
        self.set_default    = []
        self.cam_constraints_dict,self.cam_orient_dict,self.cam_sync_dict,self.cam_sync_const_dict,self.cam_stereo_dict,self.cam_stereo_eye_dict,self.cam_stereo_io_dict  = self.return_dictionaries()
        self.call_back_list              = [self.cam_constraints_dict,
                                            self.cam_orient_dict,
                                            self.cam_sync_dict,
                                            self.cam_sync_const_dict,
                                            self.cam_stereo_dict,
                                            self.cam_stereo_eye_dict,
                                            self.cam_stereo_io_dict]

        #Get the correct function and lists
        for call in self.call_back_list:
            if str(widget) in call:
                self.get_list, self.set_list, self.children, self.command = call[widget]
                self.get_default = self.get_list
                self.set_default = self.set_list[0]

        if tde4.getWidgetValue(con_man_requester,widget) ==1:
            #get the index of the get_list and apply it to the set.
            self.set_value = self.set_list[self.get_list.index(widget)]

            for sequence in self.selected_sequence_list:
                self.command(sequence,self.set_value)   #This is the actual tde4.command
            self.get_list.pop(self.get_list.index(widget)) #Removing the selected form the list. That way,

            for i,widget_name in enumerate(self.get_list):
                tde4.setWidgetValue(con_man_requester,widget_name,"0")

        else: #they've swithced it off, set to defaults (so something is "on") - which is usually the first option
            #DEFAULTS!
            for sequence in self.selected_sequence_list:
                self.command(sequence,self.set_list[0])
                tde4.setWidgetValue(con_man_requester,self.get_default[0],"1")

            self.get_default.pop(0) #Remove the Default from the list and now set the remaining in the list to 0

            for widget_name in self.get_default:
                tde4.setWidgetValue(con_man_requester,widget_name,"0")

        #check if the widget selected was "io_user", if so turn on the other one
        if widget =="self.io_user":
            tde4.setWidgetSensitiveFlag(con_man_requester, "self.io_user_input",1)
            tde4.setWidgetValue(con_man_requester,"self.io_user_input",str(self.get_stereo_io))
        else:
            tde4.setWidgetSensitiveFlag(con_man_requester, "self.io_user_input",0)



        #Turn off/off based on 'parent'
        self.parent_list = ["self.cam_constraints_none","self.sync_camera_none","self.stereo_cam_none"]

        if widget in self.parent_list:
            self.child_state = 0
        else:
            if tde4.getWidgetValue(con_man_requester,widget)==0: #If it's unlicked!
                self.child_state = 0
            else:
                self.child_state = 1
#       #Set the state of the 'children'
        for c in self.children:
            tde4.setWidgetSensitiveFlag(con_man_requester, c, self.child_state)

        #Technically, I don't want to adjust io shit to a secondary camera
        if widget == "self.stereo_cam_secondary":
            tde4.setWidgetSensitiveFlag(con_man_requester, "self.io_adjust",0)
            tde4.setWidgetSensitiveFlag(con_man_requester, "self.io_dynamic",0)
            tde4.setWidgetSensitiveFlag(con_man_requester, "self.io_user",0)
            tde4.setWidgetSensitiveFlag(con_man_requester, "self.io_user_input",0)

        tde4.setTimerCallbackFunction("gui.update_selected",300)

    def add_camera(self,con_man_requester,widget,action):

        add_camera_requester  = tde4.createCustomRequester()
        add_number            = tde4.addScaleWidget(add_camera_requester,"add_number", "Number of cameras", "INT", 1, 10, 2)
        add_req_post = tde4.postCustomRequester(add_camera_requester, "Add new cameras", 450, 80, "SEQ", "REF","CANCEL")

        if add_req_post == 1:
            for i in range(tde4.getWidgetValue(add_camera_requester,"add_number")):
                tde4.createCamera("SEQUENCE")
        elif add_req_post == 2:
            for i in range(tde4.getWidgetValue(add_camera_requester,"add_number")):
                tde4.createCamera("REF_FRAME")
#         else:
#             tde4.unpostCustomRequester(add_camera_requester)
        self.refresh_lists('con_man_requester','self.camera_list','self.lens_list')

    def del_camera(self,con_man_requester,widget,action):
        self.del_cam_req  = tde4.postQuestionRequester("Delete Cameras","You Sure? This action cannot be undone","YES","NO!")

        if self.del_cam_req ==1:

            for c in self.cam_id:
                tde4.deleteCamera(c)

            self.refresh_lists('con_man_requester','self.camera_list','self.lens_list')
        else:
            pass

    def add_pg(self,con_man_requester,widget,action):
        self.add_pg_req = tde4.postQuestionRequester("New Point Group","Select which type of point group"," OBJECT","MOCAP","CANCEL")

        if self.add_pg_req ==1:
            tde4.createPGroup("OBJECT")
        elif self.add_pg_req ==2:
            tde4.createPGroup("MOCAP")

        #refresh PG list.
        self.refresh_lists('con_man_requester','self.camera_list','self.lens_list')

    def del_pg(self,con_man_requester,widget,action):

        self.del_pg_req  = tde4.postQuestionRequester("Delete Point Group","You Sure? This action cannot be undone","YES","NO!")

        if self.del_pg_req ==1:
            for p in self.pg_id:
                tde4.deletePGroup(p)

            self.refresh_lists('con_man_requester','self.camera_list','self.lens_list')

        else:
            pass

    def add_model(self,con_man_requester,widget,value):

        if len(self.pg_id)>1:
            add_error_req = tde4.postQuestionRequester("Error","Select a single Point Group to add your obj to."," OK")
        else:

            import_obj = tde4.postFileRequester("Select Obj", "*.obj", self.model_default_path) #Set this to the correct folder for work!

            if import_obj!=None:
                model_id = tde4.create3DModel(self.pg_id[0],100000)
                model_name = os.path.basename(import_obj)
                tde4.set3DModelName(self.pg_id[0],model_id,model_name)
                tde4.importOBJ3DModel(self.pg_id[0],model_id,import_obj)
                tde4.set3DModelReferenceFlag(self.pg_id[0],model_id,1)

                tde4.insertListWidgetItem(self.con_man_requester,"self.mod_list",model_name,1) #<--CHECK IF THIS IS THE RIGHT WAY OF DOING SHIT!


    def del_model(self,con_man_requester,widget,action):
        #FIX THIS. ACTUALLY ,THIS WORKS! IT'S JUST THE MODEL LIST THAT DOESN'T!
        self.del_mod_req  = tde4.postQuestionRequester("Delete Models","You Sure? This action cannot be undone","YES","NO!")

        if self.del_mod_req ==1:
            for i,mid in enumerate(self.mod_id):
                tde4.delete3DModel(self.mod_pg_name[i],mid)

            self.refresh_lists('con_man_requester','self.camera_list','self.lens_list')
        else:
            pass

    def mod_widget_selection(self,con_man_requester,widget,value):
        #Probably not a clean way. need to reverse values and fill in forms.

        #Not allow the light to be switched off!

        if widget == 'self.mod_from_pg':
            a = tde4.getWidgetValue(self.con_man_requester,'self.mod_from_pg')
            if a == 1:
                tde4.setWidgetValue(self.con_man_requester,'self.mod_from_pg',"0")
                tde4.setWidgetValue(self.con_man_requester,'self.mod_from_all','1')
            else:
                tde4.setWidgetValue(self.con_man_requester,'self.mod_from_pg',"1")
                tde4.setWidgetValue(self.con_man_requester,'self.mod_from_all','0')
        else:
            a = tde4.getWidgetValue(self.con_man_requester,'self.mod_from_all')
            if a == 1:
                tde4.setWidgetValue(self.con_man_requester,'self.mod_from_all',"0")
                tde4.setWidgetValue(self.con_man_requester,'self.mod_from_pg','1')
            else:
                tde4.setWidgetValue(self.con_man_requester,'self.mod_from_all',"1")
                tde4.setWidgetValue(self.con_man_requester,'self.mod_from_pg','0')


    def update_selected(self):

        #Check if there's a change in camera list, pg list, lens list. If there is, run the refresh, if not break.

        if self.cam_id!=tde4.getCameraList(1) or self.pg_id!=tde4.getPGroupList(1) or self.get_lens_id_list!=tde4.getLensList(0):
            self.refresh_lists('con_man_requester','self.camera_list','self.lens_list')
            self.selected_camera('con_man_requester','self.camera_list', 'self.lens_list')
        else:
            pass
#         print "Hello"

if __name__ == '__main__':

    gui = Con_Man()