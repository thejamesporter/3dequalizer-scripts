# -*- coding: utf-8 -*-
# 3DE4.script.name:    Toggle Point Group.
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: toggles through your point groups.
#
#James Porter
#
# v1
# Feb 2013

from tde4 import *

pg	= tde4.getCurrentPGroup()
if pg!=None:
	while pg!=None:
		pg	= tde4.getNextPGroup(pg)
		if pg==None: pg = tde4.getFirstPGroup()
		tde4.setCurrentPGroup(pg)
		tde4.updateGUI()
		break