# 3DE4.script.name:    Set  -Tracking Box
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Set
# 3DE4.script.comment: Decreases the size of a tracking box until you're taking the piss.
# James Porter
# July 2014.


from tde4 import *
tde4.clearConsole()
tde4.updateGUI()
pg 	 = tde4.getCurrentPGroup()
cam 	 = tde4.getCurrentCamera()
nfr	 = tde4.getCameraNoFrames(cam)
points	 = tde4.getPointList(pg, 1)
CurFrame = tde4.getCurrentFrame(cam)

#step 1: Find the current 2D box size
values	= tde4.getPointTrackingBoxes2D(pg, points[0], cam, CurFrame)

try:
  lens = tde4.getCameraLens(cam)
  aspect = tde4.getLensPixelAspect(lens)
except:
  lens = "noLens"
  aspect = 1.0

imageWidth = float(tde4.getCameraImageWidth(cam))
imageHeight = float(tde4.getCameraImageHeight(cam))
boxS = 1.1

#step 2: Apply the new size.

search   = values[0]
pattern  = values[1]
offset   = values[2]
rotation = values[3]


boxOuterU = search[0]
boxOuterV = search[1]
boxInnerU = pattern[0]
boxInnerV = pattern[1]
offsetX   = offset[0]
offsetY   = offset[1]


boxOuterU = boxOuterU / 1.1
boxOuterV = boxOuterV / 1.1
boxInnerU = boxInnerU / 1.1
boxInnerV = boxInnerV / 1.1




#calculate center

offsetX   = boxInnerU / 2
offsetY   = boxInnerV / 2



for point in points:
  tde4.setPointTrackingBoxes2D(pg, points[0], cam, CurFrame, [boxOuterU,boxOuterV], [boxInnerU,boxInnerV], [offsetX,offsetY], rotation)