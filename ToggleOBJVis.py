# 3DE4.script.name: Toggle OBJ Visibility.
# 3DE4.script.version: v0.1
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: Toggle Obj visibility on/off

from tde4 import *

pg = tde4.getCurrentPGroup()
model = tde4.get3DModelList(pg, True)

for m in model:

    if tde4.get3DModelVisibleFlag(pg, m) == True:
        tde4.set3DModelVisibleFlag(pg, m, False)
    else:
        tde4.set3DModelVisibleFlag(pg, m, True)