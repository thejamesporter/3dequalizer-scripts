# 3DE4.script.name:    Toggle Proxy Footage
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment: Toggles Main footage, Alternative 1 etc.
# J Porter. October 2012


from tde4 import *

pg = getCurrentPGroup()
cam = getCurrentCamera()
nfr = getCameraNoFrames(cam)
points = getPointList(pg, 1)
camPro = getCameraProxyFootage(cam)
proxy = 1




if getCameraProxyFootage(cam)==0:
  proxy=1
  setCameraProxyFootage(cam,proxy)
elif getCameraProxyFootage(cam)==1:
  proxy=2
  setCameraProxyFootage(cam,proxy)
elif getCameraProxyFootage(cam)==2:
  proxy=3
  setCameraProxyFootage(cam,proxy)
elif getCameraProxyFootage(cam)==3:
  proxy=0
  setCameraProxyFootage(cam,proxy)



