# 3DE4.script.name:    Set Selected Points to New Point Set
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Set
# 3DE4.script.comment:Takes selected points and adds them to a new point set
# J Porter. 3rd July

import tde4

pg     = tde4.getCurrentPGroup()
points = tde4.getPointList(pg, 1)


def main():

    req = tde4.createCustomRequester()
    tde4.addTextFieldWidget(req,"point_set_name","Point Set name:","")

    ret=tde4.postCustomRequester(req,"New Point Set",440,120, "OK", "Cancel")

    if ret == 1:
        name = tde4.getWidgetValue(req,"point_set_name")
        set_id = tde4.createSet(pg)
        tde4.setSetName(pg,set_id,name)

        for p in points:
            tde4.setPointSet(pg,p,set_id)




main()
