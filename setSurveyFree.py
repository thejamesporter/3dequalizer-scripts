# 3DE4.script.name:    Set Survey to Free
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Set
# 3DE4.script.comment: Turns the survey of selected points to 'Free'. If you don't select any points, it switches everything to 'Free'
#
#James Porter

from tde4 import *

pg = getCurrentPGroup()
cam = getCurrentCamera()

points = getPointList(pg, 1)

if points==[]: #nowt selected
    points = getPointList(pg, 0)



for point in points:
  #if point is survey or approx, set to free. that way lineup stays as it is.
  if getPointSurveyMode(pg, point) == 'SURVEY_EXACT' or getPointSurveyMode(pg, point) == 'SURVEY_APPROX':
    setPointSurveyMode(pg, point, 'SURVEY_FREE')