# 3DE4.script.name:    Toggle Point Calc
# 3DE4.script.version: v1.0
# 3DE4.script.gui:    Main Window:: James::Toggle
# 3DE4.script.comment:Toggle's point calc between active, off and passive
# J Porter. 3rd July Toggles point calc between active, off, passive.

from tde4 import *

pg = getCurrentPGroup()
cam = getCurrentCamera()
nfr = getCameraNoFrames(cam)
points = getPointList(pg, 1)

if cam!=None and pg!=None:
    for point in points:
        if getPointCalcMode(pg, point)=='CALC_OFF':
            setPointCalcMode(pg, point, 'CALC_ACTIVE')
        elif getPointCalcMode(pg, point)=='CALC_ACTIVE':
            setPointCalcMode(pg, point, 'CALC_PASSIVE')
        elif getPointCalcMode(pg, point)=='CALC_PASSIVE':
            setPointCalcMode(pg, point, 'CALC_OFF')
