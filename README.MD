****************************************************************************
3DEqualizer Scripts.
****************************************************************************

These are various 3DEqualizer (3DE) scripts I wrote over the years.

Please feel free to use them at your own risk.

The one script which is *experiental* is conMan (connection manager). This *is
not* finished (and probably won't be as I barely used 3DE any more). Feel free
to fork it and finish it.

James Porter
2017
